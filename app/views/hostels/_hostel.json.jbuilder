json.extract! hostel, :id, :mobile_no, :data, :otp, :otp_created_at, :otp_expire, :reg_number, :created_at, :updated_at
json.url hostel_url(hostel, format: :json)
