class ChangeOtpUptdatedAtToOtpExpireAt < ActiveRecord::Migration[5.1]
  def change
    rename_column :hostels, :otp_updated_at, :otp_expired_at
  end
end
