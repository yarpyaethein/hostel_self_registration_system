class CreateRegisters < ActiveRecord::Migration[5.1]
  def change
    create_table :registers do |t|
      t.string :hostel_name
      t.string :contact_number
      t.text :address
      t.string :township
      t.string :owner_name
      t.string :owner_nric
      t.integer :number_of_rooms
      t.integer :total_pax

      t.timestamps
    end
  end
end
