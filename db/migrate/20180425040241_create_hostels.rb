class CreateHostels < ActiveRecord::Migration[5.1]
  def change
    create_table :hostels do |t|
      t.string :mobile_no ,unique: true
      t.string :data
      t.string :otp
      t.datetime :otp_created_at
      t.datetime :otp_updated_at
      t.string :reg_number

      t.timestamps
    end
  end
end
