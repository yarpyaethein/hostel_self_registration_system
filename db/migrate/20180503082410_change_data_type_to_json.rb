class ChangeDataTypeToJson < ActiveRecord::Migration[5.1]
  def change
    change_column :hostels, :data, :json, using: 'data::json'
  end
end
