require 'test_helper'

class RegisterControllerTest < ActionDispatch::IntegrationTest
  test "should get form" do
    get register_form_url
    assert_response :success
  end

  test "should get confirmation" do
    get register_confirmation_url
    assert_response :success
  end

  test "should get receipt" do
    get register_receipt_url
    assert_response :success
  end

end
