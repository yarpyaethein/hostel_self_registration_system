Rails.application.routes.draw do
  devise_for :users

  get 'register/form'

  post 'register/confirmation'

  post 'register/receipt'

  get 'register/index'

  get 'register/check'

  post 'register/check_details'

  resources :hostels
  root 'register#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
